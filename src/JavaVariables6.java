public class JavaVariables6 {
    public static void main(String[] args) throws Exception {
        int myNum = 5;
        float myFloatNum = 5.99f;
        char myLetter = 'D';
        boolean myBool = true;
        String myText = "Hello";
    }
}
