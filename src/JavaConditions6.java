public class JavaConditions6 {
    public static void main(String[] args) throws Exception {
        int time = 20;
        String result = (time < 18) ? "Good day." : "Good evening.";
        System.out.println(result);
    }
}
